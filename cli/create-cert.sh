#!/bin/bash

set -e

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[0;93m'
NC='\033[0m'

openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout mywebsite.local.key \
    -new \
    -out mywebsite.local.crt \
    -subj /CN=mywebsite.local \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /usr/lib/ssl/openssl.cnf \
        <(printf '[SAN]\nsubjectAltName=DNS:mywebsite.local')) \
    -sha256 \
    -days 3650

mkdir -p /etc/certs

mv *.crt /etc/certs/
mv *.key /etc/certs/

echo  -e ${GREEN}"Cert created in /cert! ${NC}";
